#ifndef TASK_H
#define TASK_H

#include <QRunnable>
#include <QObject>

class Task : public QObject , public QRunnable {
	
	Q_OBJECT
	
public:
	
		Task();
		void	sendFinishedSignal();
		
signals:
	
		void	finished(Task* self);
	
};

#endif
