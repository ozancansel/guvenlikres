cmake_minimum_required(VERSION 2.8.12)
project(recognitionnode)

set(CMAKE_AUTOMOC ON)

include(../third_party/dlib/dlib/cmake)

find_package(Qt5Widgets REQUIRED)
find_package(OpenCV REQUIRED)

add_executable(node main.cpp  
			webcamfeed.cpp 
			poolcontext.cpp 
			unhandledexception.cpp
			task.cpp
			findfacestask.cpp
			wrapper.cpp
			recognizefacetask.cpp)
target_link_libraries(node Qt5::Widgets)
target_link_libraries(node dlib::dlib)
target_link_libraries(node ${OpenCV_LIBS})
