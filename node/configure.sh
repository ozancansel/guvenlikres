#!/bin/bash

BUILDTYPE=Debug

if [ $# -gt 0 ]
then
	BUILDTYPE=$1
	echo "Build type is ${BUILDTYPE}"
fi

mkdir -p build
cd build 
cmake -DCMAKE_BUILD_TYPE=${BUILDTYPE} .. -DUSE_SSE4_INSTRUCTIONS=ON -DDLIB_USE_CUDA=OFF

exit
