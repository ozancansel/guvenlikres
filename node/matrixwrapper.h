#ifndef MATRIX_WRAPPER_H
#define MATRIX_WRAPPER_H

#include <dlib/matrix/matrix_abstract.h>
#include <dlib/matrix.h>
#include "wrapper.h"

using namespace dlib;

template <
        typename T,
        long num_rows = 0,
        long num_cols = 0,
        typename mem_manager = default_memory_manager,
        typename layout = row_major_layout 
        >
class MatrixWrapper : public Wrapper<matrix<T,num_rows,num_cols,mem_manager,layout> >{
	public:
	
	MatrixWrapper(matrix<T,num_rows,num_cols,mem_manager,layout> m , QObject* parent = Q_NULLPTR)
	: Wrapper<matrix<T,num_rows,num_cols,mem_manager,layout>>(m , parent)
	{
		
	}
};

#endif
