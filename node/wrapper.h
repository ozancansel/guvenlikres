#ifndef WRAPPER_H
#define WRAPPER_H

#include <QObject>

template<typename T>
class Wrapper : public QObject
{
		
public:
	
	Wrapper(T val , QObject *parent = Q_NULLPTR) : 
		QObject(parent),
		m_val(val) { }
	T	val() { return m_val; }
	
private:

	T	m_val;
	
};

#endif
