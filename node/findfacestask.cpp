#include "findfacestask.h"
#include <QThread>
#include <QDebug>
#include <dlib/opencv.h>
#include <QTime>

frontal_face_detector FindFacesTask::detector;
shape_predictor FindFacesTask::sp;
FindFacesTask::_init FindFacesTask::_initializer;

FindFacesTask::FindFacesTask(cv::Mat frame) : 
					m_frame(frame)
					{	}
					
void FindFacesTask::run(){
	QTime time;
	time.start();
	cv_image<bgr_pixel>	img(m_frame);
	m_faceRects = detector(img , 0);
	
	for(auto face : m_faceRects){
		auto shape = sp(img, face);
        matrix<bgr_pixel> face_chip;
        extract_image_chip(img, get_face_chip_details(shape,150,0.25), face_chip);
        m_faces.push_back(move(face_chip));
	}
	
	int elapsed = time.elapsed();
	qDebug() << "Detected elapsed " << elapsed << " ms" << " on " << m_frame.cols << "x" << m_frame.rows;
	sendFinishedSignal();
}

std::vector<rectangle>& FindFacesTask::faceRects(){
	return m_faceRects;
}

std::vector<matrix<bgr_pixel>>& FindFacesTask::faces(){
	return m_faces;
}
