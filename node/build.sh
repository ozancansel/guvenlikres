#!/bin/bash
CORENUM=$(nproc)
USEDCORE=$(expr $CORENUM - 1)
cd build
echo "$USEDCORE number of cores is used to build"
cmake --build . -- -j${USEDCORE}
exit
