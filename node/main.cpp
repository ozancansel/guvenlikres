#include <QApplication>
#include <QThread>
#include <QObject>
#include <QThreadPool>
#include <QDebug>
#include <dlib/image_processing.h>
#include "poolcontext.h"
#include "webcamfeed.h"
#include "findfacestask.h"
#include "matrixwrapper.h"

using namespace std;

int main(int argc, char* argv[]){
	
	QApplication	app(argc,argv);
		
	PoolContext	ctx(QThreadPool::globalInstance());
	WebcamFeed 	webcam;
	webcam.setWidth(640);
	webcam.setHeight(480);
	webcam.openCamera(0);
	webcam.feed(30);
	
	dlib::matrix<float , 0 , 1> matr;
	MatrixWrapper<float , 0 , 1> m(matr);
	
	QObject::connect(&webcam , &WebcamFeed::newFrame , &ctx , &PoolContext::feedFrame , Qt::QueuedConnection );
	
	return app.exec();
}
