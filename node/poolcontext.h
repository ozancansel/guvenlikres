#ifndef CONTEXT_H
#define CONTEXT_H

#include <QMutexLocker>
#include <QMutex>
#include <QObject>
#include <QSharedPointer>
#include <QThreadPool>
#include <QTimer>
#include <opencv2/core/core.hpp>
#include "wrapper.h"
#include "task.h"

class PoolContext : public QObject
{
	
	Q_OBJECT
	
	public:
		
		PoolContext(QThreadPool* pool , int maxTaskCount = 100 , QObject *parent = Q_NULLPTR);
		void	incrementCount();
		void	decrementCount();
		int		taskCount();
		
	public slots:
		
		void 	feedFrame(QSharedPointer<Wrapper<cv::Mat>>	frame);
		void	infoTick();
		
	private slots:
	
		void	findFacesTaskFinished(Task* t);
		void	recognizeFaceTaskFinished(Task* t);
		
	private:
	
		QMutex			m_mutex;
		QThreadPool*	m_pool;
		QTimer			m_infoTimer;
		int				m_taskCount;
		int				m_maxTaskCount;

};

#endif
