#include "unhandledexception.h"
#include <QDebug>

UnhandledException::UnhandledException(QString msg) : 
				m_message(msg)
{	}

void UnhandledException::raise() const {
	if(!m_message.isEmpty())
		qDebug() << "UnhandledException message " << m_message;
	throw *this;
}

UnhandledException* UnhandledException::clone() const {
	return new UnhandledException(*this);
}

QString UnhandledException::message() const {
	return m_message;
}
