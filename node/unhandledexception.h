#ifndef UNHANDLED_EXCEPTION_H
#define UNHANDLED_EXCEPTION_H

#include <QException>
#include <QString>

class UnhandledException : public QException
{
	
	public:
		
		UnhandledException(QString msg);
		void	raise() const;
		UnhandledException*	clone() const;
		QString 			message() const;
	
	private:
	
		QString	m_message;
	
};

#endif
