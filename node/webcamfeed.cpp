#include "webcamfeed.h"
#include "unhandledexception.h"
#include <QDebug>
#include <QTime>

WebcamFeed::WebcamFeed( QObject *parent)	:
		QThread(parent)
{
	//Timer's tick connecting to WebcamFeed::feedFrame method
	connect(&m_feedTimer , SIGNAL(timeout()) , this , SLOT(feedFrame()));
}

void WebcamFeed::openCamera(int cameraIdx){
	//Already opened
	if(m_cap.isOpened()){
		UnhandledException("Camera already opened").raise();
	}
	
	//Camera is opening
	if(!m_cap.open(cameraIdx)){	//If could not be opened
		UnhandledException("Camera could not be opened").raise();
	}
}

void WebcamFeed::feed(int fps){
	
	if(!m_cap.isOpened())
		UnhandledException("Firstly open camera").raise();
		
	//If timer already ran
	if(m_feedTimer.isActive()){
		//Stops timer
		m_feedTimer.stop();
	}
	
	//Interval calculating
	double interval = 1000.0 / fps;
	m_feedTimer.setTimerType(Qt::PreciseTimer);
	m_feedTimer.setInterval(interval);
	m_feedTimer.start();
}

void WebcamFeed::feedFrame(){
	Mat 	frame;
	m_cap.read(frame);
	Wrapper<Mat>* w = new Wrapper<Mat>(frame);
	QSharedPointer<Wrapper<Mat>> p(w);
	emit newFrame(p);
}

void WebcamFeed::stopFeed(){
	if(m_feedTimer.isActive())
		m_feedTimer.stop();
}

void WebcamFeed::closeCamera(){
	//If camera is opened
	if(m_cap.isOpened())
		m_cap.release();//Closes the camera
}

int WebcamFeed::width(){
	return m_width;
}

int WebcamFeed::height(){
	return m_height;
}

void WebcamFeed::setWidth(int width){
	m_width = width;
}

void WebcamFeed::setHeight(int height){
	m_height = height;
}
