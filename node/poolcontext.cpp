#include "poolcontext.h"
#include <opencv2/highgui/highgui.hpp>
#include <QDebug>
#include "findfacestask.h"
#include "recognizefacetask.h"

PoolContext::PoolContext(QThreadPool* pool , int maxTaskCount , QObject *parent) : 
		QObject(parent),
		m_pool(pool) ,
		m_maxTaskCount(maxTaskCount),
		m_taskCount(0)
{	
	qDebug() << "Ideal thread count is " << QThread::idealThreadCount() - 2;
	pool->setMaxThreadCount(QThread::idealThreadCount() - 2);
	m_infoTimer.setInterval(1000);
	m_infoTimer.start();
	connect(&m_infoTimer , &QTimer::timeout , this , &PoolContext::infoTick);
}

int PoolContext::taskCount(){
	QMutexLocker locker(&m_mutex);
	return m_taskCount;
}

void PoolContext::incrementCount(){
	QMutexLocker  locker(&m_mutex);	
	m_taskCount++;
}

void PoolContext::decrementCount(){
	QMutexLocker locker(&m_mutex);
	m_taskCount--;
}

void PoolContext::findFacesTaskFinished(Task* task){
	FindFacesTask*	t = (FindFacesTask*)task;
	qDebug() << t->faceRects().size() << " faces detected.";
	decrementCount();
	
	for(auto face : t->faces()){
		RecognizeFaceTask*	recognizeTask = new RecognizeFaceTask(face);
		connect(recognizeTask , &RecognizeFaceTask::finished , this , &PoolContext::recognizeFaceTaskFinished);
		recognizeTask->setAutoDelete(false);
		m_pool->start(recognizeTask , 1);
		incrementCount();
	}
	
	delete t;
}

void PoolContext::recognizeFaceTaskFinished(Task* task){
	RecognizeFaceTask* t = (RecognizeFaceTask*)task;
	decrementCount();
	qDebug() << "Recognize task finished";
	delete t;
}

void PoolContext::infoTick(){
	qDebug() << "Active thread count " << m_pool->activeThreadCount() << " Queued Tasks " << taskCount();
}

void PoolContext::feedFrame(QSharedPointer<Wrapper<cv::Mat>> frame){
	if(taskCount() >= m_maxTaskCount)
		return;
	Wrapper<cv::Mat>*	m = frame.data();
	FindFacesTask*	t = new FindFacesTask(m->val());
	t->setAutoDelete(false);
	connect(t , &FindFacesTask::finished , this , &PoolContext::findFacesTaskFinished , Qt::QueuedConnection);
	m_pool->start(t);
	incrementCount();	
}
