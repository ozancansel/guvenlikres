#ifndef WEBCAM_FEED_H
#define WEBCAM_FEED_H

#include <QObject>
#include <QTimer>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <QSharedPointer>
#include "poolcontext.h"
#include "wrapper.h"

using namespace std;
using namespace cv;

class WebcamFeed : public QThread
{
	
	Q_OBJECT
	
	public:
	
		WebcamFeed(QObject *parent = Q_NULLPTR);
		void 	openCamera(int cameraIdx = 0);	//Opens camera
		void	closeCamera();				//Closes camera
		void	feed(int fps);				//Feed frames at specified fps and slows down if context can't handle
		void	stopFeed();					//Stops feeding , doesn't closes camera
		int		width();					//Returns capturing frame width
		int		height();					//Returns capturing frame height
		void	setWidth(int width);		//Sets capturing frame width
		void	setHeight(int height);		//Sets capturing frame height
	
	public slots:
	
		void	feedFrame();				//Timer triggers this function if feed function is called
	
	signals:
	
		void	newFrame(QSharedPointer<Wrapper<Mat>> frame);		//Emits new signal
	
	private:
	
		VideoCapture	m_cap;
		QTimer			m_feedTimer;
		int				m_width;
		int				m_height;
		
};

#endif
