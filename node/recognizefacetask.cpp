#include "recognizefacetask.h"

anet_type RecognizeFaceTask::net;
RecognizeFaceTask::_init RecognizeFaceTask::_initializer;

RecognizeFaceTask::RecognizeFaceTask(matrix<bgr_pixel> face)	:
		m_face(face)
		{	}
		
void RecognizeFaceTask::run(){
	matrix<rgb_pixel>	rgb;
	assign_image(rgb , m_face);
 	m_encodings = net(rgb);
	
	sendFinishedSignal();
}

matrix<float , 0 , 1> RecognizeFaceTask::encoding(){
	return m_encodings;
}
