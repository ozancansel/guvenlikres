#ifndef FIND_FACES_TASK_H
#define FIND_FACES_TASK_H

#include <QThreadPool>
#include <QObject>
#include <opencv2/core/core.hpp>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/dnn.h>
#include "task.h"
#include "poolcontext.h"

using namespace std;
using namespace dlib;

class FindFacesTask : public Task
{
	
	public:
		
		static 	frontal_face_detector	detector;
		static 	shape_predictor			sp;
		
		static class _init
        {
          public:
            _init() {  
				detector = get_frontal_face_detector();
				deserialize("sp.dat") >> sp; 
			}
        } _initializer;
		
		FindFacesTask( cv::Mat frame );
		void	run();
		std::vector<dlib::rectangle>&			faceRects();
		std::vector<matrix<bgr_pixel>>&	faces();
		
		
	private:
	
		cv::Mat							m_frame;
		std::vector<dlib::rectangle>	m_faceRects;
		std::vector<matrix<bgr_pixel>>	m_faces;
		
};

#endif
